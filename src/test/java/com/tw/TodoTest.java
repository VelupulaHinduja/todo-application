package com.tw;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TodoTest {

    Todo todo;

    @BeforeEach
    void setUp() {
        todo = new Todo(new HashMap<>());
    }

    @Test
    void shouldVerifyTodoListExists() {
        assertTrue(todo.todoList.isEmpty());
    }

    @Test
    void shouldAddATaskToTodoList() {
        assertTrue(todo.add("Do Exercise", false));
    }

    @Test
    void shouldNotAddATaskIfAlreadyExists() {
        todo.add("Do Exercise", false);

        assertFalse(todo.add("Do Exercise", false));
    }

    @Test
    void shouldBeAbleToDeleteTaskFromList() {
        todo.add("Do Exercise", false);

        assertTrue(todo.delete("Do Exercise"));
    }

    @Test
    void shouldBeAbleToDeleteTaskFromListOnlyIfItExists() {
        assertFalse(todo.delete("Do Exercise"));
    }

    @Test
    void shouldBeAbleToChangeTheStatusOfTheTaskAsCompleted() {
        todo.add("Do Exercise", false);

        assertTrue(todo.changeStatus("Do Exercise", true));
    }

    @Test
    void shouldNotBeBeAbleToChangeTheStatusOfTheTaskAsCompletedIfItDoesNotExist() {
        assertFalse(todo.changeStatus("Do Exercise", true));
    }

    @Test
    void shouldBeAbleToChangeTheStatusOfTheTaskAsNotCompleted() {
        todo.add("Do Exercise", true);

        assertTrue(todo.changeStatus("Do Exercise", false));
    }

    @Test
    void shouldNotBeBeAbleToChangeTheStatusOfTheTaskAsNotCompletedIfItDoesNotExist() {
        assertFalse(todo.changeStatus("Do Exercise", false));
    }

    @Test
    void shouldBeAbleToEditTheTask() {
        todo.add("Do Exercise", false);

        assertTrue(todo.editTask("Do Exercise", "Do Exercise at least once"));
    }

    @Test
    void shouldNotBeAbleToEditTheTaskIfItDoesNotExists() {
        assertFalse(todo.editTask("Do Exercise", "Do Exercise at least once"));
    }

}
