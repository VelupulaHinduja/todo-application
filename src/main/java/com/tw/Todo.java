package com.tw;

import java.util.HashMap;

public class Todo {
    HashMap<String, Boolean> todoList;

    public Todo(HashMap<String, Boolean> todoList) {
        this.todoList = todoList;
    }

    public boolean add(String task, Boolean status) {
        if (todoList.containsKey(task))
            return false;
        todoList.put(task, status);
        return true;
    }

    public boolean delete(String task) {
        if (todoList.containsKey(task)) {
            todoList.remove(task);
            return true;
        }
        return false;
    }

    public boolean changeStatus(String task, Boolean status) {
        if (todoList.containsKey(task) && todoList.containsValue(!status)) {
            todoList.replace(task, todoList.get(task), status);
            return true;
        }
        return false;
    }

    public boolean editTask(String oldTask, String newTask) {
        if (todoList.containsKey(oldTask)) {
            todoList.replace(newTask, todoList.get(oldTask));
            return true;
        }
        return false;
    }
}
